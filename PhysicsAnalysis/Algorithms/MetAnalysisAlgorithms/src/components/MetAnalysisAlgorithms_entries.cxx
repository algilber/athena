// AsgExampleTools_entries.cxx

#include <MetAnalysisAlgorithms/MetBuilderAlg.h>
#include <MetAnalysisAlgorithms/MetMakerAlg.h>
#include <MetAnalysisAlgorithms/MetSignificanceAlg.h>
#include <MetAnalysisAlgorithms/MetSystematicsAlg.h>

DECLARE_COMPONENT (CP::MetBuilderAlg)
DECLARE_COMPONENT (CP::MetMakerAlg)
DECLARE_COMPONENT (CP::MetSignificanceAlg)
DECLARE_COMPONENT (CP::MetSystematicsAlg)

